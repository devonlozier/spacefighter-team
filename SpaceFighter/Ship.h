
#pragma once

#include "GameObject.h"
#include "Weapon.h"
#include "Score.h"
class Ship : public GameObject
{
public:
	Ship();
	virtual ~Ship() { }

	virtual void Update(const GameTime *pGameTime);

	virtual void Draw(SpriteBatch *pSpriteBatch) = 0; //EX Pure Virtual

	virtual void Hit(const float damage);

	virtual bool IsInvulnurable() const { return m_isInvulnurable; }

	virtual void SetInvulnurable(bool isInvulnurable = true) { m_isInvulnurable = isInvulnurable; }

	virtual std::string ToString() const { return "Ship"; }

	virtual CollisionType GetCollisionType() const = 0; //EX Pure Virtual

	virtual void AttachWeapon(Weapon *pWeapon, Vector2 position);

	virtual float GetSpeed() const { return m_speed; }

	virtual void SetSpeed(const float speed) { m_speed = speed; }

	virtual void SetMaxHitPoints(const float hitPoints) { m_maxHitPoints = hitPoints; }

	virtual void SetHitPoints(const float hitPoints) { m_hitPoints = hitPoints; }

	virtual void SetScoreAddonPoints(const int scorePoints) { m_scorePoints = scorePoints; } // assigns how much points you get when a ship is hit with a projectile
	
protected:

	virtual void Initialize();

	virtual void FireWeapons(int TextureIndexInput, TriggerType type = TriggerType::ALL);

	virtual Weapon *GetWeapon(const int index) { if (index < m_weapons.size()) return m_weapons[index]; return nullptr; }

	virtual float GetHitPoints() const { return m_hitPoints; }

	virtual float GetMaxHitPoints() const { return m_maxHitPoints; }


private:

	float m_speed;

	float m_hitPoints;
	float m_maxHitPoints;

	bool m_isInvulnurable;

	int m_scorePoints;

	std::vector<Weapon *> m_weapons;
	std::vector<Weapon *>::iterator m_weaponIt;
};

