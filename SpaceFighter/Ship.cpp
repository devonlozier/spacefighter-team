
#include "Ship.h"

Ship::Ship()
{
	SetPosition(0, 0);
	SetCollisionRadius(10);
	SetScoreAddonPoints(0);
	m_speed = 300;
	m_maxHitPoints = 1;
	m_isInvulnurable = false;

	Initialize();
}

void Ship::Update(const GameTime *pGameTime)
{
	m_weaponIt = m_weapons.begin();
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Update(pGameTime);
	}

	GameObject::Update(pGameTime);
}

void Ship::Hit(const float damage) // one issue I found is when you collide with another ship, you destroy the opposing ship and it in turn adds to the score
{
	if (!m_isInvulnurable)
	{
		m_hitPoints -= damage;

		if (m_hitPoints <= 0)
		{
			SetScore(m_scorePoints); // adds to score when ship is hit
			//std::cout << "\n" << ToString() << " | Points Added to Score: " << m_scorePoints << "\n" << "Overall Score: " << GameObject::GetScore() << "\n"; // for debugging purposes
			GameObject::Deactivate();
		}
	}
}

void Ship::Initialize()
{
	m_hitPoints = m_maxHitPoints;
}

void Ship::FireWeapons(int TextureIndexInput, TriggerType type)
{
	m_weaponIt = m_weapons.begin();

	//std::cout << "\n weapon size: " << m_weapons.size() << "\n";
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Fire(TextureIndexInput, type);
	}
}

void Ship::AttachWeapon(Weapon *pWeapon, Vector2 position)
{
	pWeapon->SetGameObject(this);
	pWeapon->SetOffset(position);
	m_weapons.push_back(pWeapon);

	//std:: cout << "\n weapon attached. Size: " << m_weapons.size() << "\n";
}