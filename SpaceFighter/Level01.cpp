

#include "Level01.h"
#include "BioEnemyShip.h"
#include "Ufo.h"
//#include "Blaster.h"
#include "conio.h"
#include "Laser.h"
#include "MathUtil.h"
#include "PowerUp.h"


void Level01::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");
	Texture *pUfoTexture = pResourceManager->Load<Texture>("Textures\\EnemyUfo.png");
	Texture *pPowerUpTexture = pResourceManager->Load<Texture>("Textures\\HealthUp.png");

	const int COUNT = 40;

	const float FIRST = Level::GetRandomXSpawn();
	const float SECOND = Level::GetRandomXSpawn();
	const float THIRD = Level::GetRandomXSpawn();
	const float FOURTH = Level::GetRandomXSpawn();
	const float FIFTH = Level::GetRandomXSpawn();
	const float SIXTH = Level::GetRandomXSpawn();
	const float SEVENTH = Level::GetRandomXSpawn();
	const float EIGHTH = Level::GetRandomXSpawn();
	const float BOSS_ONE = 0.5;

	//This isn't ideal, but is not something I can quickly fix. 
	// TODO: Set this up to use arrays and/or vectors to push various amounts of enemies and bosses.
	// TODO: Endless version with vector.

	double xPositions[COUNT] =
	{
		FIRST, FIRST-0.5, FIRST+0.5, // 0.25, 0.2, 0.3
		SECOND, SECOND+0.05, SECOND-0.05, // 0.75, 0.8, 0.7
		THIRD, THIRD-0.05, THIRD+0.05, THIRD-0.1, THIRD+0.1, // 0.3, 0.25, 0.35, 0.2, 0.4,
		FOURTH, FOURTH+0.05, FOURTH-0.05, FOURTH+0.1, FOURTH-0.1, // 0.7, 0.75, 0.65, 0.8, 0.6,
		FIFTH, FIFTH-0.1, FIFTH+0.1, FIFTH-0.05, FIFTH+0.05, // 0.5, 0.4, 0.6, 0.45, 0.55,
		SIXTH, SIXTH+0.05, SIXTH-0.05, SIXTH+0.1, SIXTH-0.1, SIXTH,
		SEVENTH, SEVENTH+0.1, SEVENTH-0.1, SEVENTH+0.15, SEVENTH-0.15, SEVENTH,
		EIGHTH, EIGHTH-0.1, EIGHTH-0.15, EIGHTH, EIGHTH+0.1, EIGHTH+0.15,
		BOSS_ONE
	};
	
	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3,
		2.0, 0.2, 0.25, 0.2, 0.25, 0.1,
		3.5, 0.15, 0.2, 0.15, 0.15, 0.2,
		4, 0.1, 0.1, 0.2, 0.1, 0.1,
		5.0
	};

	float delay = 2.0; // start delay: When enemies start spawning.
	Vector2 position;

	int powerUpIndex = (int)( (rand() % ((COUNT / 4) - ((COUNT-1) / 2)) ) + (COUNT / 4)); // generates a random index for power up to spawn

	for (int i = 0; i < COUNT; i++)
	{
		//float randNumb = (float)rand() / (float)RAND_MAX;    // Creates a random position for the X coordinate
		//float xPos = randNumb * Game::GetScreenWidth() * 0.7;
		//xPos += Game::GetScreenWidth() * 0.15;
		
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		if (i < COUNT - 1 )  
		{
			BioEnemyShip *pEnemy = new BioEnemyShip();
			pEnemy->SetTexture(pTexture);
			pEnemy->SetCurrentLevel(this);
			pEnemy->Initialize(position, (float)delay);
			AddGameObject(pEnemy);

			if(i == powerUpIndex)
			{
				PowerUp *pPowerUp = new PowerUp();
				pPowerUp->SetTexture(pPowerUpTexture);
				pPowerUp->SetCurrentLevel(this);
				pPowerUp->Initialize(position.X, (float)delay);
				AddGameObject(pPowerUp);
			}
		}
		else if (i == COUNT - 1) // 21
		{
			m_pUfo = new Ufo(100, 12, 150);
			m_pUfo->SetTexture(pUfoTexture);
			m_pUfo->SetCurrentLevel(this);
			
			Laser *pLaser = new Laser(true, 0, 0.5, false);
			pLaser->SetProjectilePool(&m_bossProjectiles);
			m_pUfo->Ship::AttachWeapon(pLaser , Vector2::UNIT_Y * 100);

			for (int i = 0; i < 100; i++)
			{
				Projectile *pProjectile = new Projectile(500, 1, Vector2::UNIT_Y, 8);
				m_bossProjectiles.push_back(pProjectile);
				AddGameObject(pProjectile);
			}

			m_pUfo->Initialize(position, (float)delay);
			//m_pUfo->Activate();
			AddGameObject(m_pUfo);

			//TODO: Figure out how to continuously spawn more enemies while the UFO is active. 
			// Idea is the boss summons adds periodically to attack the player.

			//while (m_pUfo->IsActive())
			//{
				
			//}
		
		}
	}

	Level::LoadContent(pResourceManager);
}

