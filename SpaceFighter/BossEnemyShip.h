#pragma once
#include "EnemyShip.h"

class BossEnemyShip : public EnemyShip
{

public:

	BossEnemyShip();
	virtual ~BossEnemyShip() { }

	virtual void SetTexture(Texture *pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime *pGameTime);

	virtual void Draw(SpriteBatch *pSpriteBatch);

	virtual void FireWeapons(int TextureIndexInput, TriggerType type = TriggerType::PRIMARY);

	virtual void AttachWeapon(Weapon *pWeapon, Vector2 position);

	void SetArrived(bool status);

	//virtual bool IsInvulnurable() const { return m_isInvulnurable; }

	


protected:

	Texture *m_pTexture = nullptr;

	float m_speed;

	float m_hitPoints;
	float m_maxHitPoints;

	bool m_arrived = false;

	//bool m_isInvulnurable;

	std::vector<Weapon *> m_weapons;
	std::vector<Weapon *>::iterator m_weaponIt;
};

