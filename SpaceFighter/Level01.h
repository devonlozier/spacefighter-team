#pragma once

#include "Level.h"
#include "Ufo.h"


class Level01 :	public Level
{

public:
	
	Level01() { }

	virtual ~Level01() { }

	virtual void LoadContent(ResourceManager *pResourceManager);

	virtual void UnloadContent() { }

private:

	Ufo *m_pUfo;
	std::vector<Projectile *> m_bossProjectiles;
};

