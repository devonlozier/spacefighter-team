#include "Score.h"

void Score::LoadContent(ResourceManager *pResourceManager)
{
	//Font::SetLoadSize(100, true);
	m_pFont = pResourceManager->Load<Font>("Fonts\\ariblk.ttf");

	m_text = "Score :  " + std::to_string(GetScore());
	m_color = Color::LightGreen;
	m_alpha = 1.0f;

	m_position = Vector2::ZERO;
	m_textOffset = Vector2::ZERO;

	m_textAlign = TextAlign::LEFT;	
}

void Score::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->DrawString(m_pFont, &m_text, Vector2(700, 10), m_color * m_alpha, m_textAlign);
}

void Score::Update(const GameTime *pGameTime)
{
	m_text = ("Score :  " + std::to_string(GetScore()) + GetIsSaved());//
	GameObject::Update(pGameTime);
}
